#!/usr/bin/env bash

set -exuo pipefail

# install base packages
apt-get update
apt-get install -y apt-transport-https ca-certificates curl wget software-properties-common sudo jq

# install Docker CE
apt-get remove -y docker docker-engine docker.io
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update && apt-get install -y docker-ce

# [re]create the "gcloud-config" container
curl -sSL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/project/attributes/gitlab-sa-json-key | base64 -d > /usr/local/etc/gitlab-sa-key.json
if [[ -n "$(docker ps -a | grep gcloud-config)" ]]; then
    docker rm --force --volumes gcloud-config
fi
docker run --interactive \
           --volume /usr/local/etc/gitlab-sa-key.json:/sa.json \
           --name gcloud-config \
           google/cloud-sdk \
           gcloud auth activate-service-account --key-file=/sa.json

# install GitLab runner
cat > /etc/apt/preferences.d/pin-gitlab-runner.pref <<EOF
Explanation: Prefer GitLab provided packages over the Debian native ones
Package: gitlab-runner
Pin: origin packages.gitlab.com
Pin-Priority: 1001
EOF
curl -sSL https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
apt-get -y install gitlab-runner
usermod --append --groups docker gitlab-runner

# check if this host already has a token
if [[ -z "$(curl -sSL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/project/attributes/ | grep $(hostname)-token)" ]]; then

    # find the runner registration token for the gitzup group
    GITLAB_RUNNER_REGISTRATION_TOKEN=$(curl -sSL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/project/attributes/gitlab-runner-registration-token)

    # register a new runner
    gitlab-runner register --non-interactive \
                           --url "https://gitlab.com/" \
                           --registration-token "${GITLAB_RUNNER_REGISTRATION_TOKEN}" \
                           --executor "docker" \
                           --description "$(hostname)-dind" \
                           --docker-image "docker:stable" \
                           --docker-privileged \
                           --run-untagged \
                           --locked="false"

    # extract new runner's token, and save it back to GCP metadata (to persist preemptible regenerations)
    CAPTURE_TOKEN="^  token = \"([a-zA-Z0-9]+)\"\$"
    if [[ "$(cat /etc/gitlab-runner/config.toml | grep -e '^  token = ')" =~ ${CAPTURE_TOKEN} ]]; then
        gcloud compute project-info add-metadata --metadata=$(hostname)-token=${BASH_REMATCH[1]}
    else
        echo "Could not extract GitLab runner token!" >&2
        exit 1
    fi

fi

cat > /etc/gitlab-runner/config.toml <<EOF
concurrent = 9
check_interval = 0

[[runners]]
  name = "$(hostname)-dind"
  url = "https://gitlab.com/"
  token = "$(curl -sSL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/project/attributes/$(hostname)-token)"
  executor = "docker"
  environment = ["DOCKER_DRIVER=overlay2","DOCKER_HOST=tcp://docker:2375/"]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_cache = false
    volumes = ["/cache"]
    volumes_from = ["gcloud-config:ro"]
    shm_size = 0
  [runners.cache]
EOF
